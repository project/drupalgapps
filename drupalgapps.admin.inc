<?php

/**
 * @file
 * Admin UI functions.
 */

/*
 * Admin page setting  Form has following fields
 *
 *  1. Domain information
 *      a. Domain name (validate for domain name type) (required)
 *      b. Domain name owner / Organisation using it (optional)
 *  2. Admin credentils
 *      a. Administrator email (validate for email type) (required)
 *      b. Admin password (required)
 *  3.Token renewal policy
 *      a. Renewal rate of token (default = 23)
 *      b. on event of failure
 *          i. Email (default =checked )
 *          ii. Enter the Email id (validate for email type) (optional)
 */


/**
 * Main settings form for drupalgapps.
 */
function drupalgapps_settings_form() {
  $form = array();

  $form['domain_info'] = array(
    '#type' =>'fieldset',
    '#title'=> t('Domain information'),
    '#collapsible'=> TRUE,
    '#collapsed' => FALSE,
  );

  $form['domain_info']['domain_name'] = array(
    '#type' => t('textfield'),
    '#title'=> t('Domain name'),
    '#description' => t('Your Google Apps domain name, e.g. www.example.com'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('drupalgapps_domain_name', ''),
  );

  $form['domain_info']['organisation'] = array(
    '#type' => t('textfield'),
    '#title'=> t('Organisation Name'),
    '#description' => t('Used for display purposes only.'),
    '#size' => 30,
    '#default_value' => variable_get('drupalgapps_organisation_name', ''),
  );

  $form['admin_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Administrator credentials'),
    '#description' => t('Administrator account details to use for fetching Authentication token.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['admin_credentials']['admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Preferably you should create a separate email id with admin privileges which is not in use elsewhere. To prevent problems, the password for this administration account should not be changed without updating here too.'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => trim(aes_decrypt(variable_get('drupalgapps_admin_email', ''))),
  );

  $form['admin_credentials']['admin_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password for the admin email'),
    '#required' => TRUE,
    '#size' => 30,
    '#default_value' => trim(aes_decrypt(variable_get('drupalgapps_admin_password', ''))),
  );

  $form['token_policy'] = array(
    '#type' => 'fieldset' ,
    '#title' => t('Token Renewal Policy'),
    '#collapsible'=> TRUE,
    '#collapsed' => FALSE,
  );

  $form['token_policy']['renewal_rate'] = array(
    '#type' => 'select',
    '#title' => t('Renewal rate of authentication token (in hours)'),
    '#options' => drupal_map_assoc(range(1, 23)),
    '#default_value' => variable_get('drupalgapps_token_renewal_rate', 23),
    '#description' => t("ONLY FOR ADVANCED USERS.<br/>Do not change without knowledge about provisioning API authentication.<br/><br/> The authenication token used for Google's Provisioning API expires every 24 hours, but to prevent problems, the module renews the token in every 23 hrs by default."),
  );

  $form['token_policy']['on_failure'] = array(
    '#type' => 'fieldset' ,
    '#title'=> t('Error handling'),
    '#description' => t('Action to be taken when the module is unable to fetch the Authentication token for Google Apps'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['token_policy']['on_failure']['send_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send Email to Administrator'),
    '#default_value' => variable_get('drupalgapps_email_send_option', 1),
    '#description' => t('Drupal will email administrator if token expires, gets revoked or for any other problems which prevents use of the provisioning API.'),
  );

  $form['token_policy']['on_failure']['failure_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Warning email address'),
    '#description' => t('If blank, the email will be sent to the administrator account used to fetch the authentication token.'),
    '#size' => 30,
    '#required' => FALSE,
    '#default_value' => variable_get('drupalgapps_failure_email', ''),
  );

  $form['proxy_config'] = array(
    '#type' => 'fieldset',
    '#title'=> t('Proxy configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['proxy_config']['proxy'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy Server Details'),
    '#description' => t('If your webserver is behind a proxy server, enter proxy details in the format <em>[ip address]:[port]</em> , eg : 172.16.68.6:3128'),
    '#size' => 30,
    '#required' => FALSE,
    '#default_value' => variable_get('drupalgapps_proxy', ''),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validation function for drupalgapps_settings_form().
 */
function drupalgapps_settings_form_validate($form, &$form_state) {
  $admin_email = valid_email_address($form_state['values']['admin_email']);
  $failure_email = valid_email_address($form_state['values']['failure_email']);

  if (!$admin_email) {
    form_set_error('admin_email', t('Email address entered is invalid.'));
  }
  elseif ($form_state['values']['failure_email'] && !$failure_email) {
    form_set_error('failure_email', t('Email address entered is invalid.'));
  }
}


/**
 * Submit handler for drupalgapps_settings_form().
 */
function drupalgapps_settings_form_submit($form, &$form_state) {
  variable_set('drupalgapps_domain_name', trim($form_state['values']['domain_name']));
  variable_set('drupalgapps_organisation_name', trim($form_state['values']['organisation']));
  variable_set('drupalgapps_token_renewal_rate', $form_state['values']['renewal_rate']);
  variable_set('drupalgapps_email_send_option', $form_state['values']['send_email']);
  variable_set('drupalgapps_failure_email', trim($form_state['values']['failure_email']));
  variable_set('drupalgapps_proxy', trim($form_state['values']['proxy']));

  $encrypted_admin_email = aes_encrypt(trim($form_state['values']['admin_email']));
  variable_set('drupalgapps_admin_email', $encrypted_admin_email);

  $encrypted_admin_password = aes_encrypt(trim($form_state['values']['admin_password']));
  variable_set('drupalgapps_admin_password', $encrypted_admin_password);

  // Fetch the authentication token.
  drupalgapps_get_authtoken();
}

