<?php

/**
 * @file
 * Admin UI functions.
 */

/**
 * Main settings form for drupalgapps ui.
 */
function drupalgapps_ui_settings_form() {
  $form = array();

  // Automatic provisioning settings.
  $form['automatic_provisioning'] = array(
    '#type' =>'fieldset',
    '#title' => t('Automatic account creation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['automatic_provisioning']['drupalgapps_ui_provision_new_account_on_login'] = array(
    '#type' => t('checkbox'),
    '#title'=> t('Provision new Google Apps account on login'),
    '#description' => t("A new Google Apps account will be created for each user when they next login, if they do not have one already.  The user will need the 'obtain google apps account' permission."),
    '#default_value' => variable_get('drupalgapps_ui_provision_new_account_on_login', FALSE),
  );

  // New account notification settings.
  $form['notification_new_account_settings'] = array(
    '#type' =>'fieldset',
    '#title' => t('Notification settings: new account'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['notification_new_account_settings']['drupalgapps_ui_notify_new_account'] = array(
    '#type' => t('checkbox'),
    '#title' => t('Notify users of new Google Apps account'),
    '#default_value' => variable_get('drupalgapps_ui_notify_new_account', TRUE),
  );
  $form['notification_new_account_settings']['drupalgapps_ui_new_account_subject'] = array(
    '#type' => t('textfield'),
    '#title' => t('Subject'),
    '#required' => FALSE,
    '#default_value' => variable_get('drupalgapps_ui_new_account_subject', 'Your new Google Apps account has been created'),
  );
  $form['notification_new_account_settings']['drupalgapps_ui_new_account_body'] = array(
    '#type' => t('textarea'),
    '#title' => t('Message'),
    '#size' => 30,
    '#required' => FALSE,
    '#default_value' => variable_get('drupalgapps_ui_new_account_body', ''),
  );
  $format_default = variable_get('drupalgapps_ui_new_account_body_filter', FILTER_FORMAT_DEFAULT);
  $form['notification_new_account_settings']['drupalgapps_ui_new_account_body_filter']['drupalgapps_ui_new_account_body_format']
    = filter_form($format_default, NULL, array('drupalgapps_ui_new_account_body_format'));

  $form['notification_new_account_settings']['token'] = array(
    '#collapsed'      => TRUE,
    '#collapsible'    => TRUE,
    '#title'          => t('Replacement values'),
    '#type'           => 'fieldset',
  );
  $form['notification_new_account_settings']['token']['token_help'] = array(
    '#value' => theme('token_help', 'drupalgapps_ui'),
  );

  return system_settings_form($form);
}


/**
 * Create new Google Apps account form.
 */
function drupalgapps_ui_create_account_form() {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#value' => '<p>' . t('Create new Google Apps accounts for entered users or users with selected roles.') . '</p>',
  );

  $form['users'] = array(
    '#type' => 'textfield',
    '#title' => t('Usernames'),
    '#autocomplete_path' => 'user/autocomplete',
  );

  // Provide roles checkboxes.
  $roles = array();
  $result = db_query('SELECT rid, name FROM {role} WHERE rid != 1 ORDER BY name');
  while ($row = db_fetch_object($result)) {
    $roles[$row->rid] = $row->name;
  }
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#options' => $roles,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Google Accounts'),
  );

  return $form;
}

/**
 * Validation handler; create new Google Apps account.
 */
function drupalgapps_ui_create_account_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['users'])) {
    $users = preg_split('/\s*,\s*/', $form_state['values']['users']);
    foreach ($users as $username) {
      $uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s' AND status=1", $username));
      if (!$uid) {
        form_set_error('users', t("No active user with the username '%username' found.", array('%username' => $username)));
      }
    }
  }
}

/**
 * Submit handler; create new Google Apps account.
 */
function drupalgapps_ui_create_account_form_submit($form, &$form_state) {
  // Usernames entered.
  if (!empty($form_state['values']['users'])) {
    $users = preg_split('/\s*,\s*/', $form_state['values']['users']);

    // For each user entered, create an account if necessary.
    foreach ($users as $username) {
      $account = user_load(array('name' => $username));
      drupalgapps_ui_create_drupal_user_gapps_account($account);
    }
  }

  // Roles selected.
  if (!empty($form_state['values']['roles'])) {
    $roles = array();
    foreach ($form_state['values']['roles'] as $rid => $checked) {
      if ($checked) {
        $roles[] = $rid;
      }
    }
    if (!empty($roles)) {
      // Fetch all active users with the selected roles.
      $placeholders = db_placeholders($roles, 'int');
      $result = db_query("SELECT r.uid FROM {users_roles} r, {users} u WHERE u.uid = r.uid AND u.status = 1 AND r.rid IN (". $placeholders . ")", $roles);
      while ($row = db_fetch_object($result)) {
        $account = user_load($row->uid);
        drupalgapps_ui_create_drupal_user_gapps_account($account);
      }
    }
  }
}

